package hello;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.LdapShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.anyRequest().fullyAuthenticated()
				.and()
			.formLogin();
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.ldapAuthentication()
				//.userDnPatterns("uid={0}")
				.groupSearchBase("ou=Usuarios,ou=Oficina") 
				.contextSource()
					.url("ldap://10.0.61.10:389/dc=sensebyte,dc=com")
					.managerDn("Externo01@SENSEBYTE").managerPassword("123456789")
					.and()
				//.passwordCompare()
				//	.passwordEncoder(new LdapShaPasswordEncoder())
				//	.passwordAttribute("userPassword");
				.userSearchFilter("(&(objectClass=user)(cn={0}))")
				;
	}

}
